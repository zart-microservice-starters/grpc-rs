use super::*;
#[test]
fn test_creates_message() {
    assert_eq!(
        "Hello Tonic from hellors",
        create_message("Tonic".to_string())
    );
}
