mod domain;
#[cfg(test)]
mod domain_test;
mod utils;

use tonic::{transport::Server, Request, Response, Status};

use crate::domain::create_message;
use crate::utils::{load_config, setup_logger};
use hello_rs::greeter_server::{Greeter, GreeterServer};
use hello_rs::{HelloReply, HelloRequest};
use log::info;

pub mod hello_rs {
    tonic::include_proto!("hellors");
}

#[derive(Debug, Default)]
pub struct MyGreeter {}

#[tonic::async_trait]
impl Greeter for MyGreeter {
    async fn say_hello(
        &self,
        request: Request<HelloRequest>,
    ) -> Result<Response<HelloReply>, Status> {
        let reply = hello_rs::HelloReply {
            message: create_message(request.into_inner().name),
        };

        Ok(Response::new(reply))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = load_config().unwrap();
    setup_logger(&config.log_level).unwrap();
    info!("Running with: {:?}", config);
    let _guard = sentry::init((
        config.sentry_url,
        sentry::ClientOptions {
            release: sentry::release_name!(),
            ..Default::default()
        },
    ));

    let addr = format!("{}:{}", config.host, config.port);
    let greeter = MyGreeter::default();
    Server::builder()
        .add_service(GreeterServer::new(greeter))
        .serve(addr.parse().unwrap())
        .await?;
    Ok(())
}
