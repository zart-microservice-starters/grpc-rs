use fern::colors::{Color, ColoredLevelConfig};
use serde::{Deserialize, Serialize};
use std::env;
use std::net::Ipv4Addr;
use validator::Validate;

#[derive(Validate, Debug, Deserialize, Serialize)]
pub struct Config {
    pub log_level: String,
    #[validate(range(min = 80))]
    pub port: u16,
    pub host: Ipv4Addr,
    #[validate(url)]
    pub sentry_url: Option<String>,
    pub e2e: bool,
}

#[derive(Debug, Deserialize, Serialize)]
struct ConfigMap {
    local: Config,
    development: Config,
    beta: Config,
    production: Config,
}

pub fn load_config() -> Result<Config, String> {
    let f = std::fs::File::open("cfg.yml").expect("Could not open file.");
    let config_map: ConfigMap = serde_yaml::from_reader(f).expect("Could not read values.");
    let environment = env::var("ENV").unwrap_or_else(|_| "local".to_string());
    let config = match environment.as_str() {
        "local" => config_map.local,
        "development" => config_map.development,
        "beta" => config_map.beta,
        _ => config_map.production,
    };
    match config.validate() {
        Ok(_) => (),
        Err(e) => panic!("{}", e),
    };
    Ok(config)
}

// #[allow(dead_code)]
pub fn setup_logger(log_level: &str) -> Result<(), fern::InitError> {
    let colors = ColoredLevelConfig::new()
        .info(Color::Green)
        .debug(Color::Blue)
        .error(Color::Red)
        .warn(Color::Yellow);
    let level = log_level.to_uppercase();
    fern::Dispatch::new()
        .filter(|metadata| metadata.target() == "grpc")
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{}: {}",
                colors.color(record.level()),
                message
            ))
        })
        .level(match level.as_str() {
            "ERROR" => log::LevelFilter::Error,
            "WARN" => log::LevelFilter::Warn,
            "INFO" => log::LevelFilter::Info,
            "DEBUG" => log::LevelFilter::Debug,
            _ => panic!("Logger only accepts ERROR, WARN, INFO, and DEBUG levels"),
        })
        .chain(std::io::stdout())
        .apply()?;
    Ok(())
}
