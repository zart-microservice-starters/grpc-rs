use std::sync::{Arc, Mutex};
use std::time::Duration;

use domain::create_message;
use futures_util::FutureExt;
use hello_rs::greeter_client::GreeterClient;
use hello_rs::greeter_server::{Greeter, GreeterServer};
use hello_rs::{HelloReply, HelloRequest};
use tokio::sync::oneshot;
use tokio::task::JoinHandle;
use tonic::transport::{Channel, Server};
use tonic::{Request, Response, Status};
pub mod hello_rs {
    tonic::include_proto!("hellors");
}

struct Svc(Arc<Mutex<Option<oneshot::Sender<()>>>>);

#[tonic::async_trait]
impl Greeter for Svc {
    async fn say_hello(
        &self,
        request: Request<HelloRequest>,
    ) -> Result<Response<HelloReply>, Status> {
        let mut l = self.0.lock().unwrap();
        l.take().unwrap().send(()).unwrap();
        let reply = HelloReply {
            message: create_message(request.into_inner().name),
        };

        Ok(Response::new(reply))
    }
}
async fn init() -> (GreeterClient<Channel>, JoinHandle<()>) {
    let (tx, rx) = oneshot::channel();
    let sender = Arc::new(Mutex::new(Some(tx)));
    let svc = GreeterServer::new(Svc(sender));
    let server = tokio::spawn(async move {
        Server::builder()
            .add_service(svc)
            .serve_with_shutdown("127.0.0.1:1338".parse().unwrap(), rx.map(drop))
            .await
            .unwrap();
    });

    tokio::time::sleep(Duration::from_millis(0)).await;

    let client = GreeterClient::connect("http://127.0.0.1:1338")
        .await
        .unwrap();
    (client, server)
}

#[tokio::test]
async fn test_message_response() {
    let (mut client, server) = init().await;
    let request = Request::new(HelloRequest {
        name: "test".into(),
    });

    let response = client.say_hello(request).await.unwrap();

    assert_eq!("Hello test from hellors", response.into_inner().message);
    server.await.unwrap();
}
