# grpc-rs 🦀

![](https://img.shields.io/gitlab/pipeline-status/zart-microservice-starters/grpc-rs?branch=development&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/zart-microservice-starters/grpc-rs/development?logo=rust&style=for-the-badge)
